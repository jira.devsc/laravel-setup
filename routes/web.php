<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PortalController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Main Page Route
Route::get('/', [PortalController::class, 'home'])->name('home');

/* Route Profile Pages */
Route::group(['prefix' => 'profile'], function () {
    Route::get('about', [PortalController::class, 'about'])->name('profile-about');
});
/* Route Profile Pages */

/* Route Error Pages */
Route::get('/error', [PortalController::class, 'error'])->name('page-error');
