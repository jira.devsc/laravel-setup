<!DOCTYPE html>
<html lang="en">

<head>
    @include('template.head')
</head>

<body>
    <div class="content-wrapper">
        @include('template.header')
        @yield('body')
        @yield('js')
        @include('template.footer')
    </div>
    @include('template.main_js')
</body>

</html>