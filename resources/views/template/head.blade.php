<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="">
    <meta name="author" content="elemis">
    <title>@yield('title') - Ciptadrasoft</title>
    <link rel="shortcut icon" href="{{asset('assets/img/ciptadra/favicon.png')}}">
    <link rel="stylesheet" href="{{asset('assets/css/plugins.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/colors/purple.css')}}">
</head>