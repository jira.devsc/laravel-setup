<?php

namespace App\Http\Controllers;

use Helpers;
use Config;
use Illuminate\Http\Request;

class PortalController extends Controller
{
    // Configurasi Helper To API
    public function __construct()
    {
        $this->api_url = Config::get('app.api_url');
        $this->helper = new Helpers();
    }

    // Home - Index
    public function Home()
    {
        // SAMPEL API dan Pemanggilannya
        $data["slider"] = $this->helper->api("GET", $this->api_url . "/ViewPortal/getSlider?siteId=100&typeId=SL01&status=ST01&fileType=FL02", null, null, null);

        return view('home', $data);
    }
}
