<?php

namespace App\Helpers;

class Helpers
{
    public function api($method, $url, $t, $token = null, $data = null)
    {
        if ($t  == 'json') {
            $type = 'application/json';
        } else if ($t == 'form') {
            $type = 'multipart/form-data';
        } else {
            $type = 'application/json';
        }
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/18.17763');
        if (!empty($token)) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: ' . $type, 'Authorization: Bearer ' . $token));
        } else {
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: ' . $type));
        }

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);

        switch ($method) {
            case "GET":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
                break;
            case "POST":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "DELETE":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
        }
        $curl_response = curl_exec($curl);
        curl_close($curl);
        return json_decode($curl_response);
    }
}
